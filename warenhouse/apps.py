from django.apps import AppConfig


class WarenhouseConfig(AppConfig):
    name = 'warenhouse'
