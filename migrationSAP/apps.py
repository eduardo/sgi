from django.apps import AppConfig


class MigrationsapConfig(AppConfig):
    name = 'migrationSAP'
