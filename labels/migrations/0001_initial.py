# Generated by Django 2.1.3 on 2022-05-22 23:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('orders', '0004_auto_20211018_1600'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalLabel',
            fields=[
                ('id_label', models.IntegerField(blank=True, db_index=True)),
                ('initial_range', models.CharField(max_length=15, verbose_name='Rango Unicial')),
                ('end_range', models.CharField(max_length=15, verbose_name='Rango Unicial')),
                ('quantity', models.IntegerField(default=0, verbose_name='Cantidad')),
                ('parcial', models.SmallIntegerField(default=0, verbose_name='Parcial')),
                ('notas', models.TextField(blank=True, default=None, null=True)),
                ('bg_status', models.CharField(choices=[('I', 'Inactive'), ('S', 'Sended'), ('A', 'Active'), ('D', 'Disabled')], default='I', max_length=15, verbose_name='Estado')),
                ('id_user', models.SmallIntegerField(default=0)),
                ('date_created', models.DateTimeField(blank=True, editable=False, verbose_name='Fecha de Creación')),
                ('last_updated', models.DateTimeField(blank=True, editable=False, verbose_name='Ultima Actualización')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('id_factura_detalle', models.ForeignKey(blank=True, db_column='detalle_pedido_factura', db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='orders.OrderInvoiceDetail')),
            ],
            options={
                'verbose_name': 'historical label',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='Label',
            fields=[
                ('id_label', models.AutoField(primary_key=True, serialize=False)),
                ('initial_range', models.CharField(max_length=15, verbose_name='Rango Unicial')),
                ('end_range', models.CharField(max_length=15, verbose_name='Rango Unicial')),
                ('quantity', models.IntegerField(default=0, verbose_name='Cantidad')),
                ('parcial', models.SmallIntegerField(default=0, verbose_name='Parcial')),
                ('notas', models.TextField(blank=True, default=None, null=True)),
                ('bg_status', models.CharField(choices=[('I', 'Inactive'), ('S', 'Sended'), ('A', 'Active'), ('D', 'Disabled')], default='I', max_length=15, verbose_name='Estado')),
                ('id_user', models.SmallIntegerField(default=0)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Fecha de Creación')),
                ('last_updated', models.DateTimeField(auto_now=True, verbose_name='Ultima Actualización')),
                ('id_factura_detalle', models.ForeignKey(db_column='detalle_pedido_factura', on_delete=django.db.models.deletion.CASCADE, to='orders.OrderInvoiceDetail')),
            ],
        ),
    ]
