Modulo de liquidaciones de parciales:

Por Hacer

<ul>
<li>
    <input type="checkbox">
Migrar modulo de prorrateos
</li>
<li>
        <input type="checkbox">
Costos de arrastre, similares a bodega del parcial
</li>
<li>
        <input type="checkbox">
Permitir liquidar un pedido desde cualquier parcial incluso si existen parciales anteriores cerrados
</li>
<li>
        <input type="checkbox">
La liquidacion en cada uno de los parciales debe realizarse en una sola pantalla sin importar el numero de parcial que se vaya a liquidar
</li>
<li>
        <input type="checkbox">
En la liquidacion de cada parcial se debe mostrar solamente las provisiones que estan por cargar al mayor SGI
</li>
<li>
        <input type="checkbox">
Dividir los gastos iniciales y de cada uno de los parciales en pestañas laterales para que se simplifique la negavcion en los costos de los pedidos</li>
<li>
        <input type="checkbox">
Tener una lista de los prorrateos aplicados al costeo en cada uno de los parciales</li>
</ul>


ALTER TABLE `gastos_nacionalizacion` CHANGE `date_create` `date_create` TIMESTAMP(6) NULL DEFAULT CURRENT_TIMESTAMP(6);
ALTER TABLE `producto` CHANGE `nro_registro_sanitario` `nro_registro_sanitario` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

