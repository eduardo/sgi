from django.apps import AppConfig


class AlmagroConfig(AppConfig):
    name = 'almagro'
