from django.apps import AppConfig


class CostingsConfig(AppConfig):
    name = 'costings'
