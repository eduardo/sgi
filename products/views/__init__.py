from .ProductDetailView import ProductDetailView
from .ProductsListView import ProductListView
from .ProductUpdateView import ProductUpdateView
from .ProductCreateView import ProductCreateView